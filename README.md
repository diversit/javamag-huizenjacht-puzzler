# Java Magazine 'Op huizenjacht' puzzler oplossing

Implementatie in Scala.
Checkout project en run 'sbt run'

Enige dependency is dat SBT geinstalleerd is. Ik gebruik SBT 0.13.

## Oplossing
Gebruik maken van de som formule van Gauss volgt dat de oplossing van deze puzzler gevonden kan worden door de formule:

H = Huisnr  
L = Laatste Huisnr

SOM(1 tot H-1) == SOM(1 tot L) - SOM(1 tot H)

1/2 (H-1)(H-1+1) == 1/2 (L)(L+1) - 1/2 (H)(H+1)

Hieruit volgt dat H berekend kan worden uit L via:

H = sqrt( 1/2 (L)(L+1) )

De oplossing itereert over waarde voor L en berekent H. Indien H een geheel getal is, dan is dat een oplossing.

In code:
~~~~
  new LongIterator(3)
    .map { lastNr => (lastNr, calcHouseNr(lastNr)) }
    .filter { case (_, houseNr) => isWholeNr(houseNr) }
    .take(20)
    .foreach { case (lastNr, houseNr) => println("houseNr: %d, lastNr: %d" format (houseNr.toInt, lastNr)) }
~~~~