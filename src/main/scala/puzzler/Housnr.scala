package puzzler

object Housnr extends App {

  val start = System.currentTimeMillis()

  new LongIterator(3)
    .map { lastNr => (lastNr, calcHouseNr(lastNr)) }
    .filter { case (_, houseNr) => isWholeNr(houseNr) }
    .take(20)
    .foreach { case (lastNr, houseNr) => println("houseNr: %d, lastNr: %d" format (houseNr.toInt, lastNr)) }

  val end = System.currentTimeMillis()
  println("Calculated in %d millies" format (end-start))

  def isWholeNr(nr: Double): Boolean = nr.toInt == nr

  def calcHouseNr(lastNr: Long): Double = Math.sqrt( (lastNr * (lastNr + 1)) / 2)

}

class LongIterator(val start: Long) extends Iterator[Long] {

  var current: Long = start

  override def hasNext = current != Long.MaxValue

  override def next() = {
    current = current + 1
    current
  }
}


